/**
 * Copyright (C) 2009 Klaus Reimer <k@ailis.de>
 * See LICENSE.TXT for licensing information
 * 
 * @fileoverview
 * Provides the view assistant for the game view.
 * 
 * @author Klaus Reimer (k@ailis.de)
 */


/**
 * Constructs the game view assistant.
 * 
 * @constructor
 * @extends Mojo.Assistant.SceneAssistant
 * @class The game view assistant.
 */

function GameAssistant()
{
    this.prefs = Preferences.getInstance();
    
    destroids.msgTitle = $L("app.name");
    destroids.msgNewGame = $L("gameView.newGame");
    destroids.msgContinueGame = $L("gameView.continueGame");
    destroids.msgPreferences = $L("gameView.preferences");
    destroids.msgHelp = $L("gameView.help");
    destroids.msgRightOn = $L("gameView.rightOn");
    destroids.msgNextLevel = $L("gameView.nextLevel");
    destroids.msgGameOver = $L("gameView.gameOver");
    destroids.msgShield = $L("gameView.shield");
    destroids.msgHull = $L("gameView.hull");
    destroids.msgScore = $L("gameView.score");
    destroids.msgLevel = $L("gameView.level");
    destroids.msgShortLevel = $L("gameView.shortLevel");
    destroids.msgRank = $L("gameView.rank");
    destroids.msgName = $L("gameView.name");
    destroids.msgNewHighScoreTitle = $L("gameView.newHighScoreTitle");
    destroids.msgNewHighScore = $L("gameView.newHighScore");
    destroids.msgNewHighScoreWithLocal = $L("gameView.newHighScoreWithLocal");
    destroids.msgRankNotification = $L("gameView.rankNotification");
    destroids.msgRankErrorNotification = $L("gameView.rankErrorNotification");
    destroids.msgThousandSep = $L("gameView.thousandSep");
    destroids.msgGlobalIndicator = $L("gameView.globalIndicator");
    destroids.msgLocalIndicator = $L("gameView.localIndicator");
    destroids.msgEject = $L("gameView.eject");
    destroids.msgEjected = $L("gameView.ejected");
    
    destroids.onPreferences = this.openPreferences.bind(this);
    destroids.onHelp = this.openHelp.bind(this);
    destroids.onPrompt = this.prompt.bind(this);
    destroids.onSound = this.playSound.bind(this);
    destroids.scoreSubmitUrl = "http://scority.ailis.de/submit/destroids";
    destroids.scoreTop5Url = "http://scority.ailis.de/top5/destroids";
    destroids.onNotification = this.notify.bind(this);
    
    this.game = new destroids.Game("game", false);
}

/**
 * The preferences. 
 * @private 
 * @type {!Preferences} 
 */
GameAssistant.prototype.prefs;

/** 
 * The game. 
 * @private 
 * @type {!destroids.Game} 
 */
GameAssistant.prototype.game;

/** 
 * If game is started for first time. 
 * @private 
 * @type {boolean} 
 */
GameAssistant.prototype.firstStart = true;

/** 
 * The last time a sound was played. 
 * @private 
 * @type {number} 
 */
GameAssistant.prototype.lastSoundTime = 0;

/** 
 * The last played sound ID. 
 * @private 
 * @type {number}
 */ 
GameAssistant.prototype.lastSound = -1;


/**
 * Setup the view.
 * 
 * @param {Mojo.Event} event
 *            The mojo event
 * @override
 */

GameAssistant.prototype.setup = function(event)
{
    // Translate view
    $$(".i18n").each(/** @param {Element} e */ function(e) { e.update($L(e.innerHTML)).addClassName("resolved"); });

    // Setup application menu
    this.controller.setupWidget(Mojo.Menu.appMenu,
        {
            omitDefaultItems: false
        },
        {
            visible: true,
            items:
            [ 
                { label: $L("appMenu.about"), command: "about" },
                { label: $L("appMenu.forum"), command: "forum" },
                { label: $L("appMenu.donation"), command: "donation" },
                { label: $L("appMenu.resetHighScores"), command: "resetHighScores" }
            ]
        }
    );
    
    // Register events to suspend the game on minimize and resume it on
    // maximimize
    Mojo.Event.listen(this.controller.stageController.document,
            Mojo.Event.stageDeactivate,
            this.suspend.bindAsEventListener(this));
    Mojo.Event.listen(this.controller.stageController.document,
            Mojo.Event.stageActivate,
            this.resume.bindAsEventListener(this));
};


/**
 * Activate the view.
 * 
 * @param {Mojo.Event} event
 *            The mojo event
 * @override
 */

GameAssistant.prototype.activate = function(event)
{
    // Enable free screen rotation
    if (this.controller.stageController.setWindowOrientation)
        this.controller.stageController.setWindowOrientation(this.prefs.orientation);

    // Enable screen blocking
    if (this.controller.stageController.setWindowProperties)
        this.controller.stageController.setWindowProperties({
            blockScreenTimeout: this.prefs.blockScreenTimeout
        });

    // Setup full screen mode
    this.controller.enableFullScreenMode(this.prefs.fullScreen);
    
    // Setup game controls
    destroids.ctrlThrust = [ this.prefs.ctrlThrust ];
    destroids.ctrlEject = [ this.prefs.ctrlEject ];
    destroids.ctrlRight = [ this.prefs.ctrlRight ];
    destroids.ctrlLeft = [ this.prefs.ctrlLeft ];
    destroids.ctrlFire = [ this.prefs.ctrlFire ];
    destroids.ctrlMenu = [ this.prefs.ctrlMenu ];
    destroids.ctrlRollRange = this.prefs.ctrlRollRange;
    destroids.ctrlRollDeadZone = this.prefs.ctrlRollDeadZone;
    destroids.ctrlPitchRange = this.prefs.ctrlPitchRange;
    destroids.ctrlPitchDeadZone = this.prefs.ctrlPitchDeadZone;
    destroids.ctrlRotationCompensator = this.prefs.ctrlRotationCompensator;

    // Start the game. When this is the first start then delay the call
    // because otherwise for some strange the app menu does not appear
    if (this.firstStart)
    {
        this.game.start.bind(this.game).delay(0.5);
        this.firstStart = false;
    }
    else
    {
        this.game.start();
    }
};


/**
 * Deactivate the view.
 * 
 * @param {Mojo.Event} event
 *            The mojo event
 * @override
 */

GameAssistant.prototype.deactivate = function(event)
{
    // Stop the game
    this.game.stop();
};


/**
 * Cleanup the view.
 * 
 * @param {Mojo.Event} event
 *            The mojo event
 * @override
 */

GameAssistant.prototype.cleanup = function(event)
{
    // Empty
};


/**
 * Opens the preferences view.
 */

GameAssistant.prototype.openPreferences = function()
{
    Mojo.Controller.stageController.pushScene("preferences");
};


/**
 * Opens the help view.
 */

GameAssistant.prototype.openHelp = function()
{
    Mojo.Controller.stageController.pushScene("help");
};


/**
 * Suspends the game (if enabled in preferences)
 * 
 * @private
 */  
   
GameAssistant.prototype.suspend = function()
{
    if (this.prefs.suspend) this.game.stop();
};


/**
 * Resumes the game.
 * 
 * @private
 */  
   
GameAssistant.prototype.resume = function() 
{
    if (this.prefs.suspend) this.game.start();
};


/**
 * Prompts for a value
 * 
 * @param {string} title
 *            The dialog title
 * @param {string} message
 *            The dialog message
 * @param {Function} onSubmit
 *            The callback function to call with the entered value
 * @param {Object} context
 *            The context to use for calling the callback function
 * @private
 */

GameAssistant.prototype.prompt = function(title, message, onSubmit,
    context)
{
    PromptAssistant.run(this, title, message, onSubmit, context);
};


/**
 * Displays a message as a notification
 * 
 * @param {string} message
 *            The message to display
 * @private
 */

GameAssistant.prototype.notify = function(message)
{
    Mojo.Controller.getAppController().showBanner(
    {
        "messageText": message
    }, "", "rank");
};


/**
 * Listens for a back swipe and opens the menu if not already open.
 * 
 * @param {Mojo.Event} event
 *            The mojo event
 * @override
 */

GameAssistant.prototype.handleCommand = function(event)
{
    var game;
    
    if (event.type == Mojo.Event.back && !this.game.isMenuOpen())
    {
        this.game.gotoMenu();
        event.preventDefault();
    }

    else if (event.type == Mojo.Event.command)
    {
        switch (event.command)
        {
            case "resetHighScores":
                game = this.game;
                this.controller.showAlertDialog(
                {
                    onChoose: /** @param {string} value} */ function(value)
                    {
                        if (value == "yes") (/** @type {destroids.Game} */ game).resetHighScores();
                    },
                    title: $L("dialog.confirm"),
                    message: $L("dialog.reallyResetHighScores"),
                    choices:
                    [
                        { label: $L("button.yes"), value: "yes", type: "affirmative" },
                        { label: $L("button.no"), value: "no", type: "negative" }
                    ]
                });
                break;
        }
    }
};


/**
 * Plays sound effects. Currently this just plays system sounds.
 * Unfortunately Palm did a really bad job implementing the HTML 5
 * audio tag so I can't play real sounds...
 * 
 * @param {number} sound
 *            The id of the sound to play
 */

GameAssistant.prototype.playSound = function(sound)
{
    var name, now;

    if (this.prefs.sound)
    {
        // Get current timestamp
        now = new Date().getTime();
    
        // Play sound only if new sound has higher priority or if
        // last sound play was 250 ms ago 
        if (sound > this.lastSound || this.lastSoundTime + 250 < now)
        {
            // Remember which sound was played and when
            this.lastSoundTime = now;
            this.lastSound = sound;
        
            switch (sound)
            {
                /* Disabled because keypresses are repeated and this sounds bad          
                case destroids.SND_SPACESHIP_THRUST:
                    name = "appclose";
                    break;
                */
                    
                case destroids.SND_SPACESHIP_FIRE:
                    name = "back_01";
                    break;
        
                case destroids.SND_UFO_FIRE:
                    name = "delete_01";
                    break;
                    
                case destroids.SND_SMALL_ASTEROID_DESTROYED:
                    name = "card_01";
                    break;
                
                case destroids.SND_LARGE_ASTEROID_DESTROYED:
                    name = "card_02";
                    break;
        
                case destroids.SND_UFO_HULL_DAMAGE:
                    name = "down2";
                    break;
                
                case destroids.SND_UFO_DESTROYED:
                    name = "shuffling_01";
                    break;
                
                case destroids.SND_DROP_DESTROYED:
                    name = "error_02";
                    break;
        
                case destroids.SND_SPACESHIP_SHIELD_DAMAGE:
                    // No sound
                    break;
        
                case destroids.SND_SPACESHIP_HULL_DAMAGE:
                    // No sound
                    break;
        
                case destroids.SND_SPACESHIP_DESTROYED:
                    name = "shuffle_07";
                    break;
                
                case destroids.SND_EJECT:
                    name = "shuffle_05";
                    break;
                
                case destroids.SND_COLLECT_DROP:
                    name = "browser_01";
                    break;
                    
                case destroids.SND_LEVEL_UP:
                    name = "browser_01";
                    break;
                    
                default:
                    name = null;
            }
    
            // Play the sound
            if (name)
            {
                this.controller.serviceRequest("palm://com.palm.audio/systemsounds",
                {
                    method: "playFeedback",
                    parameters: { name: name } 
                });
            }
        }
    }

    // Process vibrate feedback
    if (this.prefs.vibrate)
    {
        switch (sound)
        {
            case destroids.SND_SPACESHIP_DESTROYED:
            case destroids.SND_SPACESHIP_SHIELD_DAMAGE:
            case destroids.SND_SPACESHIP_HULL_DAMAGE:
                Mojo.Controller.getAppController().playSoundNotification(
                        "vibrate", "")
        }
    }
};
