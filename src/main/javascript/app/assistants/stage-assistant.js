/**
 * Copyright (C) 2009 Klaus Reimer <k@ailis.de>
 * See LICENSE.TXT for licensing information
 * 
 * @fileoverview
 * Provides the stage assistant.
 * 
 * @author Klaus Reimer (k@ailis.de)
 */


/**
 * Constructs the stage assistant.
 * 
 * @constructor
 * @extends Mojo.Assistant.StageAssistant
 * @class The stage assistant.
 */

function StageAssistant ()
{
    // Empty
}


/**
 * Setup the stage assistant.
 * 
 * @override
 */

StageAssistant.prototype.setup = function()
{
    this.controller.pushScene("game");
};
