/**
 * Copyright (C) 2009 Klaus Reimer <k@ailis.de>
 * See LICENSE.TXT for licensing information
 * 
 * @fileoverview
 * Provides the view assistant for the "about" view.
 * 
 * @author Klaus Reimer (k@ailis.de)
 */


/**
 * Constructs the about view assistant.
 * 
 * @constructor
 * @extends Mojo.Assistant.SceneAssistant
 * @class The about view assistant.
 */

function AboutAssistant()
{
    // Empty
}


/**
 * Setup the view.
 * 
 * @param {Mojo.Event} event
 *            The mojo event
 * @override
 */

AboutAssistant.prototype.setup = function(event)
{
    // Translate view
    $$(".i18n").each(/** @param {Element} e */ function(e) { e.update($L(e.innerHTML)).addClassName("resolved"); });
       
    // Setup application menu
    this.controller.setupWidget(Mojo.Menu.appMenu,
        {
            omitDefaultItems: false
        },
        {
            visible: true,
            items:
            [ 
                { label: $L("appMenu.about"), command: "about", disabled: true },
                { label: $L("appMenu.forum"), command: "forum" },
                { label: $L("appMenu.donation"), command: "donation" }
            ]
        }
    );    
};


/**
 * Activate the view.
 * 
 * @param {Mojo.Event} event
 *            The mojo event
 * @override
 */

AboutAssistant.prototype.activate = function(event)
{
    // Empty
};



/**
 * Deactivate the view.
 * 
 * @param {Mojo.Event} event
 *            The mojo event
 * @override
 */

AboutAssistant.prototype.deactivate = function(event)
{
    // Empty
};


/**
 * Cleanup the view.
 * 
 * @param {Mojo.Event} event
 *            The mojo event
 * @override
 */

AboutAssistant.prototype.cleanup = function(event)
{
    // Empty
};
