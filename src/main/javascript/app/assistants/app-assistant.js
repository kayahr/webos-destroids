/**
 * Copyright (C) 2009 Klaus Reimer <k@ailis.de>
 * See LICENSE.TXT for licensing information
 * 
 * @fileoverview
 * Provides the application assistant.
 * 
 * @author Klaus Reimer (k@ailis.de)
 */


/**
 * Constructs the application assistant.
 * 
 * @constructor
 * @extends Mojo.Assistant.AppAssistant
 * @class The application assistant.
 */

function AppAssistant ()
{
    // Empty
}



/**
 * Handles commands send from the app menu.
 * 
 * @param {Mojo.Event} event
 *            The mojo event
 * @override
 */

AppAssistant.prototype.handleCommand = function(event)
{
    var sceneName;
    
    sceneName = this.controller.getActiveStageController().activeScene().sceneName;
    if (event.type == Mojo.Event.commandEnable)
    {
        switch (event.command)
        {
            case Mojo.Menu.helpCmd:
                if (sceneName != "help") event.stopPropagation();
                break;
                
            case Mojo.Menu.prefsCmd:
                if (sceneName != "preferences") event.stopPropagation();
                break;
        }
    }

    if (event.type == Mojo.Event.command)
    {
        switch (event.command)
        {
            case "about":
                Mojo.Controller.stageController.pushScene("about");
                break;
                
            case Mojo.Menu.prefsCmd:
                Mojo.Controller.stageController.pushScene("preferences");
                break;

            case Mojo.Menu.helpCmd:
                Mojo.Controller.stageController.pushScene("help");
                break;

            case "donation":
                new Mojo.Service.Request("palm://com.palm.applicationManager",
                {
                    method: "open",
                    parameters:
                    {
                        id: "com.palm.app.browser",
                        params:
                        {
                            target: $L("app.donationUrl")
                        }
                    }
                }); 
                break;
                
            case "forum":
                new Mojo.Service.Request("palm://com.palm.applicationManager",
                {
                    method: "open",
                    parameters:
                    {
                        id: "com.palm.app.browser",
                        params:
                        {
                            target: $L("app.forumUrl")
                        }
                    }
                }); 
                break;
        }
    }
};
