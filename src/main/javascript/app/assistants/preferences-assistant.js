/**
 * Copyright (C) 2009 Klaus Reimer <k@ailis.de>
 * See LICENSE.TXT for licensing information
 * 
 * @fileoverview
 * Provides the view assistant for the "preferences" view.
 * 
 * @author Klaus Reimer (k@ailis.de)
 */


/**
 * Constructs the preferences view assistant.
 * 
 * @constructor
 * @extends Mojo.Assistant.SceneAssistant
 * @class The preferences view assistant.
 */

function PreferencesAssistant()
{
    this.prefs = Preferences.getInstance();
    this.updateSliderValuesHandler = this.updateSliderValues.bind(this);
}

/** 
 * The preferences. 
 * @private
 * @type {!Preferences} 
 */
PreferencesAssistant.prototype.prefs;

/** 
 * The update slider values handler. 
 * @private
 * @type {Function} 
 */
PreferencesAssistant.prototype.updateSliderValuesHandler = null;

/**
 * The roll range model.
 * @private
 * @type {!Mojo.Model.SliderModel}
 */
PreferencesAssistant.prototype.rollRangeModel;

/**
 * The roll dead zone model.
 * @private
 * @type {!Mojo.Model.SliderModel}
 */
PreferencesAssistant.prototype.rollDeadZoneModel;

/**
 * The pitch range model.
 * @private
 * @type {!Mojo.Model.SliderModel}
 */
PreferencesAssistant.prototype.pitchRangeModel;

/**
 * The pitch dead zone model.
 * @private
 * @type {!Mojo.Model.SliderModel}
 */
PreferencesAssistant.prototype.pitchDeadZoneModel;


/**
 * Setup the view.
 * 
 * @param {Mojo.Event} event
 *            The mojo event
 * @override
 */
PreferencesAssistant.prototype.setup = function(event)
{
    var ctrlChoices, ctrlCode, ctrl;
    
    // Translate view
    $$(".i18n").each(/** @param {Element} e */ function(e) { e.update($L(e.innerHTML)).addClassName("resolved")});
    
    ctrlChoices = [];
    for ((/** @type {string} */ ctrl) in (/** @type {Object.<string, number>} */ Preferences.CTRLMAP))
    {
        ctrlCode = Preferences.CTRLMAP[ctrl];
        ctrlChoices.push({
            label: Preferences.getCtrlName(ctrlCode),
            value: ctrlCode 
        });
    }

    // Setup the control selectors
    this.controller.setupWidget("ctrlLeftSelector",
        {
            label: $L("preferencesView.ctrlLeft"),
            choices: ctrlChoices,
            modelProperty: "ctrlLeft"
        },
        this.prefs
    );
    this.controller.setupWidget("ctrlRightSelector",
        {
            label: $L("preferencesView.ctrlRight"),
            choices: ctrlChoices,
            modelProperty: "ctrlRight"
        },
        this.prefs
    );
    this.controller.setupWidget("ctrlThrustSelector",
            {
                label: $L("preferencesView.ctrlThrust"),
                choices: ctrlChoices,
                modelProperty: "ctrlThrust"
            },
            this.prefs
        );
    this.controller.setupWidget("ctrlEjectSelector",
            {
                label: $L("preferencesView.ctrlEject"),
                choices: ctrlChoices,
                modelProperty: "ctrlEject"
            },
            this.prefs
        );
    this.controller.setupWidget("ctrlFireSelector",
        {
            label: $L("preferencesView.ctrlFire"),
            choices: ctrlChoices,
            modelProperty: "ctrlFire"
        },
        this.prefs
    );
    this.controller.setupWidget("ctrlMenuSelector",
        {
            label: $L("preferencesView.ctrlMenu"),
            choices: ctrlChoices,
            modelProperty: "ctrlMenu"
        },
        this.prefs
    );
    
    // Setup rotation compensator toggle button
    this.controller.setupWidget("rotationCompensatorButton",
        {
            trueLabel: $L("button.yes"),
            falseLabel: $L("button.no"),
            trueValue: true,
            falseValue: false,
            modelProperty: "ctrlRotationCompensator"
        },
        this.prefs
    );

    this.controller.setupWidget("rollRangeSlider",
        {
            minValue: 0,
            maxValue: 180,
            round: true,
            updateInterval: 0.1
        },
        this.rollRangeModel = { value: this.prefs.ctrlRollRange }
    );
    this.controller.listen("rollRangeSlider", Mojo.Event.propertyChange,
            this.updateSliderValuesHandler);
    this.controller.setupWidget("rollDeadZoneSlider",
        {
            minValue: 0,
            maxValue: 180,
            round: true,
            updateInterval: 0.1
        },
        this.rollDeadZoneModel = { value: this.prefs.ctrlRollDeadZone }
    );
    this.controller.listen("rollDeadZoneSlider", Mojo.Event.propertyChange,
            this.updateSliderValuesHandler);
    
    this.controller.setupWidget("pitchRangeSlider",
        {
            minValue: 0,
            maxValue: 180,
            round: true,
            updateInterval: 0.1
        },
        this.pitchRangeModel = { value: this.prefs.ctrlPitchRange }
    );
    this.controller.listen("pitchRangeSlider", Mojo.Event.propertyChange,
            this.updateSliderValuesHandler);
    this.controller.setupWidget("pitchDeadZoneSlider",
        {
            minValue: 0,
            maxValue: 180,
            round: true,
            updateInterval: 0.1
        },
        this.pitchDeadZoneModel = { value: this.prefs.ctrlPitchDeadZone }
    );
    this.controller.listen("pitchDeadZoneSlider", Mojo.Event.propertyChange,
            this.updateSliderValuesHandler);

    this.updateSliderValues();
    
    // Setup sound toggle button
    this.controller.setupWidget("soundButton",
        {
            trueLabel: $L("button.yes"),
            falseLabel: $L("button.no"),
            trueValue: true,
            falseValue: false,
            modelProperty: "sound"
        },
        this.prefs
    );

    // Setup sound vibrate button
    this.controller.setupWidget("vibrateButton",
        {
            trueLabel: $L("button.yes"),
            falseLabel: $L("button.no"),
            trueValue: true,
            falseValue: false,
            modelProperty: "vibrate"
        },
        this.prefs
    );

    // Setup fullscreen toggle button
    this.controller.setupWidget("fullScreenButton",
        {
            trueLabel: $L("button.yes"),
            falseLabel: $L("button.no"),
            trueValue: true,
            falseValue: false,
            modelProperty: "fullScreen"
        },
        this.prefs
    );
    
    // Setup block screen timeout toggle button
    this.controller.setupWidget("blockScreenTimeoutButton",
        {
            trueLabel: $L("button.yes"),
            falseLabel: $L("button.no"),
            trueValue: true,
            falseValue: false,
            modelProperty: "blockScreenTimeout"
        },
        this.prefs
    );

    // Setup the orientation selector
    this.controller.setupWidget("orientationSelector",
        {
            label: $L("preferencesView.orientation"),
            choices: 
            [
                { label: $L("preferencesView.orientation.free"), value: "free" },
                { label: $L("preferencesView.orientation.up"), value: "up" },
                { label: $L("preferencesView.orientation.right"), value: "right" },
                { label: $L("preferencesView.orientation.down"), value: "down" },
                { label: $L("preferencesView.orientation.left"), value: "left" }
            ],
            modelProperty: "orientation"
        },
        this.prefs
    );
    
    // Setup suspend-on-minimize toggle button
    this.controller.setupWidget("suspendButton",
        {
            trueLabel: $L("button.yes"),
            falseLabel: $L("button.no"),
            trueValue: true,
            falseValue: false,
            modelProperty: "suspend"
        },
        this.prefs
    );
    
    // Setup application menu
    this.controller.setupWidget(Mojo.Menu.appMenu,
        {
            omitDefaultItems: false
        },
        {
            visible: true,
            items:
            [ 
                { label: $L("appMenu.about"), command: "about" },
                { label: $L("appMenu.forum"), command: "forum" },
                { label: $L("appMenu.donation"), command: "donation" },
                { label: $L("appMenu.resetPrefs"), command: "reset" }
            ]
        }
    );
};


/**
 * Updates the slider values.
 * 
 * @private
 */

PreferencesAssistant.prototype.updateSliderValues = function()
{
    this.prefs.ctrlRollRange = this.rollRangeModel.value;
    this.prefs.ctrlRollDeadZone = this.rollDeadZoneModel.value;
    this.prefs.ctrlPitchRange = this.pitchRangeModel.value;
    this.prefs.ctrlPitchDeadZone = this.pitchDeadZoneModel.value;
    $("rollRangeValue").innerHTML = this.prefs.ctrlRollRange;        
    $("rollDeadZoneValue").innerHTML = this.prefs.ctrlRollDeadZone;
    $("pitchRangeValue").innerHTML = this.prefs.ctrlPitchRange;        
    $("pitchDeadZoneValue").innerHTML = this.prefs.ctrlPitchDeadZone;
    this.controller.modelChanged(this.prefs, this);    
};


/**
 * Activate the view.
 *
 * @param {Mojo.Event} event
 *            The mojo event
 * @override
 */

PreferencesAssistant.prototype.activate = function(event)
{
    // Empty
};


/**
 * Deactivate the view.
 *
 * @param {Mojo.Event} event
 *            The mojo event
 * @override
 */

PreferencesAssistant.prototype.deactivate = function(event)
{
    // Empty
};


/**
 * Cleanup the view.
 *
 * @param {Mojo.Event} event
 *            The mojo event
 * @override
 */

PreferencesAssistant.prototype.cleanup = function(event)
{
    this.prefs.save();
};


/**
 * Handles commands send from a menu.
 * 
 * @param {Mojo.Event} event
 *           The mojo event
 * @override
 */

PreferencesAssistant.prototype.handleCommand = function(event)
{
    if (event.type == Mojo.Event.command)
    {
        switch (event.command)
        {
            case "reset":
                this.prefs.reset();
                this.rollRangeModel.value = this.prefs.ctrlRollRange;
                this.rollDeadZoneModel.value = this.prefs.ctrlRollDeadZone;
                this.pitchRangeModel.value = this.prefs.ctrlPitchRange;
                this.pitchDeadZoneModel.value = this.prefs.ctrlPitchDeadZone;
                this.controller.modelChanged(this.prefs, this);
                this.controller.modelChanged(this.rollRangeModel, this);
                this.controller.modelChanged(this.rollDeadZoneModel, this);
                this.controller.modelChanged(this.pitchRangeModel, this);
                this.controller.modelChanged(this.pitchDeadZoneModel, this);
                this.updateSliderValues();
                break;
        }
    }
};
