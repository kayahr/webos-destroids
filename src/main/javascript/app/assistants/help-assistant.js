/**
 * Copyright (C) 2009 Klaus Reimer <k@ailis.de>
 * See LICENSE.TXT for licensing information
 * 
 * @fileoverview
 * Provides the view assistant for the "help" view.
 * 
 * @author Klaus Reimer (k@ailis.de)
 */


/**
 * Constructs the help view assistant.
 * 
 * @constructor
 * @extends Mojo.Assistant.SceneAssistant
 * @class The help view assistant.
 */

function HelpAssistant()
{
    // Empty
}


/**
 * Setup the view.
 * 
 * @param {Mojo.Event} event
 *            The mojo event
 * @override 
 */

HelpAssistant.prototype.setup = function(event)
{
    // Translate view
    $$(".i18n").each(/** @param {Element} e */ function(e) { e.update($L(e.innerHTML)).addClassName("resolved"); });
       
    // Setup application menu
    this.controller.setupWidget(Mojo.Menu.appMenu,
        {
            omitDefaultItems: false
        },
        {
            visible: true,
            items:
            [ 
                { label: $L("appMenu.about"), command: "about" },
                { label: $L("appMenu.forum"), command: "forum" },
                { label: $L("appMenu.donation"), command: "donation" }
            ]
        }
    );    
};


/**
 * Activate the view.
 * 
 * @param {Mojo.Event} event
 *            The mojo event
 * @override 
 */

HelpAssistant.prototype.activate = function(event)
{
    // Empty
};



/**
 * Deactivate the view.
 * 
 * @param {Mojo.Event} event
 *            The mojo event
 * @override 
 */

HelpAssistant.prototype.deactivate = function(event)
{
    // Empty
};


/**
 * Cleanup the view.
 * 
 * @param {Mojo.Event} event
 *            The mojo event
 * @override 
 */

HelpAssistant.prototype.cleanup = function(event)
{
    // Empty
};
