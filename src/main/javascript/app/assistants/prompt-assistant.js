/**
 * Copyright (C) 2009 Klaus Reimer <k@ailis.de>
 * See LICENSE.TXT for licensing information
 * 
 * @fileoverview
 * Provides the view assistant for the prompt dialog.
 * 
 * @author Klaus Reimer (k@ailis.de)
 */


/**
 * Constructs the prompt dialog.
 * 
 * @param {Mojo.Assistant.SceneAssistant} sceneAssistant
 *            The scene assistant
 * @param {string} title
 *            The dialog title
 * @param {string} message
 *            The dialog message
 * @param {Function} onSubmit
 *            The callback function to call with the entered value
 * @param {Object} context
 *            The context to use for calling the callback function
 * 
 * @constructor
 * @extends Mojo.Assistant.DialogAssistant
 * @class The prompt dialog.
 */

function PromptAssistant(sceneAssistant, title, message, onSubmit, context)
{
    this.sceneAssistant = sceneAssistant;
    this.title = title;
    this.message = message;
    this.onSubmit = onSubmit;
    this.context = context;
    this.valueModel = { value: "", disabled: false };
    this.okHandler = this.handleOk.bind(this);
}

/**
 * The scene assistant.
 * @private
 * @type {Mojo.Assistant.SceneAssistant}
 */
PromptAssistant.prototype.sceneAssistant;

/**
 * The widget.
 * @private
 * @type {Mojo.Widget}
 */
PromptAssistant.prototype.widget;

/**
 * The dialog title.
 * @private
 * @type {string}
 */
PromptAssistant.prototype.title;

/**
 * The dialog message.
 * @private
 * @type {string}
 */
PromptAssistant.prototype.message;

/**
 * The onSubmit handler.
 * @private
 * @type {Function}
 */
PromptAssistant.prototype.onSubmit;

/**
 * The onSubmit handler context.
 * @private
 * @type {Object}
 */
PromptAssistant.prototype.context;

/**
 * The OK handler.
 * @private
 * @type {Function}
 */
PromptAssistant.prototype.okHandler;

/**
 * The value model.
 * @private
 * @type {Mojo.Model.TextFieldModel}
 */
PromptAssistant.prototype.valueModel;


/**
 * Setup the view.
 * 
 * @param {Mojo.Widget} widget
 *            The widget
 *            
 */

PromptAssistant.prototype.setup = function(widget)
{
    this.widget = widget;
    
    // Translate view
    $$(".i18n").each(/** @param {Element} e */ function(e) { e.update($L(e.innerHTML)).addClassName("resolved"); });

    // Set messages
    $("dialogTitle").innerHTML = this.title;
    $("dialogMessage").innerHTML = this.message;

    // Setup text field for entering the name
    this.sceneAssistant.controller.setupWidget("valueTextField",
        {
            maxLength: 32,   
            autoFocus: true,
            autoReplace: false,
            focusMode: Mojo.Widget.focusSelectMode            
        },
        this.valueModel);

    // Setup the OK button
    this.sceneAssistant.controller.setupWidget("okButton",
        {
        },
        {
            label: $L("button.ok")
        });
    this.sceneAssistant.controller.listen("okButton", Mojo.Event.tap,
        this.okHandler);    
};


/**
 * Activate the view.
 */

PromptAssistant.prototype.activate = function(event)
{
    // Empty
};


/**
 * Deactivate the view.
 */

PromptAssistant.prototype.deactivate = function(event)
{
    // Empty
};


/**
 * Cleanup the view.
 */

PromptAssistant.prototype.cleanup = function(event)
{
    this.sceneAssistant.controller.stopListening("okButton",
            Mojo.Event.tap, this.okHandler);
};


/**
 * Handles the OK button press.
 * 
 * @private
 */

PromptAssistant.prototype.handleOk = function()
{
    this.onSubmit.call(this.context, this.valueModel.value);
    this.widget.mojo.close();
};


/**
 * Runs this dialog.
 * 
 * @param {Mojo.Assistant.SceneAssistant} sceneAssistant
 *            The scene assistant
 * @param {string} title
 *            The dialog title
 * @param {string} message
 *            The dialog message
 * @param {Function} onSubmit
 *            The callback function to call with the entered value
 * @param {Object} context
 *            The context to use for calling the callback function
 */

PromptAssistant.run = function(sceneAssistant, title, message, onSubmit,
    context)
{
    sceneAssistant.controller.showDialog({
        template: "dialogs/prompt-dialog",
        preventCancel: true,
        assistant: new PromptAssistant(sceneAssistant, title, message,
            onSubmit, context)
    });
};
