/**
 * Copyright (C) 2009 Klaus Reimer <k@ailis.de>
 * See LICENSE.TXT for licensing information
 * 
 * @fileoverview
 * Provides the preferences model.
 * 
 * @author Klaus Reimer (k@ailis.de)
 */


/**
 * Constructs a new instance of the preferences model. This must not be done
 * manually. Always call the static getInstance() method.
 * 
 * @constructor
 * @class The preferences model.
 */

function Preferences()
{    
    this.load();
}

/**
 * The ctrlmap.
 * @final
 * @type {Object.<string, number>} 
 */
Preferences.CTRLMAP = {
    "$touch": -1,
    "$rollLeft": -2,
    "$rollRight": -3,
    "$pitchDown": -4,
    "$pitchUp": -5,
    "A": 65,
    "B": 66,
    "C": 67,
    "D": 68,
    "E": 69,
    "F": 70,
    "G": 71,
    "H": 72,
    "I": 73,
    "J": 74,
    "K": 75,
    "L": 76,
    "M": 77,
    "N": 78,
    "O": 79,
    "P": 80,
    "Q": 81,
    "R": 82,
    "S": 83,
    "T": 84,
    "U": 85,
    "V": 86,
    "W": 87,
    "X": 88,
    "Y": 89,
    "Z": 90,
    "\u2190": 8,
    "\u21b5": 13,
    ",": 188,
    "Sym": 17,
    ".": 190,
    "$space": 32,
    "@": 48,
    "\u2191": 16
};

/** 
 * The singleton instance of the preferences model.
 * @private 
 * @type {Preferences} 
 */
Preferences.instance = null;

/** 
 * The control for rotating ship to the left. 
 * @type {number} 
 */
Preferences.prototype.ctrlLeft = Preferences.CTRLMAP["K"]; 

/** 
 * The control for rotating ship to the left. 
 * @type {number} 
 */
Preferences.prototype.ctrlRight = Preferences.CTRLMAP["L"]; 

/** 
 * The control for rotating ship to the left. 
 * @type {number} 
 */
Preferences.prototype.ctrlThrust = Preferences.CTRLMAP["A"]; 

/** 
 * The control for ejecting from the ship. 
 * @type {number} 
 */
Preferences.prototype.ctrlEject = Preferences.CTRLMAP["E"]; 

/** 
 * The control for rotating ship to the left. 
 * @type {number} 
 */
Preferences.prototype.ctrlFire = Preferences.CTRLMAP["Q"]; 

/** 
 * The control for rotating ship to the left. 
 * @type {number} 
 */
Preferences.prototype.ctrlMenu = Preferences.CTRLMAP["$touch"]; 

/** 
 * The pitch range. 
 * @type {number} 
 */
Preferences.prototype.ctrlPitchRange = 45;

/** 
 * The pitch dead zone. 
 * @type {number} 
 */
Preferences.prototype.ctrlPitchDeadZone = 5;

/** 
 * The roll range. 
 * @type {number} 
 */
Preferences.prototype.ctrlRollRange = 45;

/** 
 * The roll dead zone. 
 * @type {number} 
 */
Preferences.prototype.ctrlRollDeadZone = 5;

/** 
 * If app should run in full screen mode. 
 * @type {boolean} 
 */
Preferences.prototype.fullScreen = true;

/** 
 * The window orientation. 
 * @type {string} 
 */
Preferences.prototype.orientation = "free";

/** 
 * If screen timeout should be blocked. 
 * @type {boolean} 
 */
Preferences.prototype.blockScreenTimeout = false;

/** 
 * If game should suspend on minimize. 
 * @type {boolean} 
 */
Preferences.prototype.suspend = true;

/** 
 * If rotation compensator should be used. 
 * @type {boolean} 
 */
Preferences.prototype.ctrlRotationCompensator = true;

/** 
 * If sounds should be played. 
 * @type {boolean} 
 */
Preferences.prototype.sound = true;

/** 
 * If phone should vibrate on impact. 
 * @type {boolean} 
 */
Preferences.prototype.vibrate = true;


/**
 * Returns the control name for the specified control code.
 * 
 * @param {number} ctrlCode
 *            The control code
 * @return {?string} The control name. NUll if not found.
 */

Preferences.getCtrlName = function(ctrlCode)
{
    var ctrl;
    
    for ((/** @type {string} */ ctrl) in (/** @type {Object.<string, number>} */ Preferences.CTRLMAP))
    {
        if (Preferences.CTRLMAP[ctrl] == ctrlCode)
            if (ctrl.charAt(0) == "$")
                return $L("controls." + ctrl.substring(1));
            else
                return ctrl;
    }
    return null;
};


/**
 * Returns the singleton instance of the preferences model.
 * 
 * @return {!Preferences} The preferences model
 */

Preferences.getInstance = function()
{
    if (!Preferences.instance) Preferences.instance = new Preferences();
    return Preferences.instance;
};


/**
 * Loads the preferences.
 * 
 * @private
 */

Preferences.prototype.load = function()
{
    var cookie, data;
    
    cookie = new Mojo.Model.Cookie("Preferences").get();
    this.reset();
    if (cookie)
    {
        if (cookie["fullScreen"] != null)
            this.fullScreen = !!cookie["fullScreen"];
        if (cookie["orientation"] != null)
            this.orientation = String(cookie["orientation"]);
        if (cookie["blockScreenTimeout"] != null)
            this.blockScreenTimeout = !!cookie["blockScreenTimeout"];
        if (cookie["suspend"] != null)
            this.suspend = !!cookie["suspend"];
        if (cookie["ctrlLeft"] != null)
            this.ctrlLeft = +cookie["ctrlLeft"];
        if (cookie["ctrlRight"] != null)
            this.ctrlRight = +cookie["ctrlRight"];
        if (cookie["ctrlFire"] != null)
            this.ctrlFire = +cookie["ctrlFire"];
        if (cookie["ctrlThrust"] != null)
            this.ctrlThrust = +cookie["ctrlThrust"];
        if (cookie["ctrlEject"] != null)
            this.ctrlEject = +cookie["ctrlEject"];
        if (cookie["ctrlMenu"] != null)
            this.ctrlMenu = +cookie["ctrlMenu"];
        if (cookie["ctrlRotationCompensator"] != null)
            this.ctrlRotationCompensator = !!cookie["ctrlRotationCompensator"];

        if (cookie["ctrlPitchRange"] != null)
            this.ctrlPitchRange = +cookie["ctrlPitchRange"];
        if (cookie["ctrlPitchDeadZone"] != null)
            this.ctrlPitchDeadZone = +cookie["ctrlPitchDeadZone"];
        
        if (cookie["ctrlRollRange"] != null)
            this.ctrlRollRange = +cookie["ctrlRollRange"];
        if (cookie["ctrlRollDeadZone"] != null)
            this.ctrlRollDeadZone = +cookie["ctrlRollDeadZone"];

        if (cookie["sound"] != null)
            this.sound = !!cookie["sound"];
        if (cookie["vibrate"] != null)
            this.vibrate = !!cookie["vibrate"];
    }
};


/**
 * Saves the preferences.
 */

Preferences.prototype.save = function()
{
    new Mojo.Model.Cookie("Preferences").put({
        version: 1,
        fullScreen: this.fullScreen,
        orientation: this.orientation,
        blockScreenTimeout: this.blockScreenTimeout,
        suspend: this.suspend,
        ctrlLeft: this.ctrlLeft,
        ctrlRight: this.ctrlRight,
        ctrlThrust: this.ctrlThrust,
        ctrlEject: this.ctrlEject,
        ctrlFire: this.ctrlFire,
        ctrlMenu: this.ctrlMenu,
        ctrlPitchRange: this.ctrlPitchRange,
        ctrlPitchDeadZone: this.ctrlPitchDeadZone,
        ctrlRollRange: this.ctrlRollRange,
        ctrlRollDeadZone: this.ctrlRollDeadZone,
        ctrlRotationCompensator: this.ctrlRotationCompensator,
        sound: this.sound,
        vibrate: this.vibrate
    });
};


/**
 * Resets the preferences to factory defaults.
 */

Preferences.prototype.reset = function()
{
    this.fullScreen = true;
    this.blockScreenTimeout = false;
    this.orientation = "free";    
    this.suspend = true;    
    this.ctrlLeft = Preferences.CTRLMAP["K"]; 
    this.ctrlRight = Preferences.CTRLMAP["L"]; 
    this.ctrlThrust = Preferences.CTRLMAP["A"]; 
    this.ctrlEject = Preferences.CTRLMAP["E"]; 
    this.ctrlFire = Preferences.CTRLMAP["Q"]; 
    this.ctrlMenu = Preferences.CTRLMAP["$touch"];
    this.ctrlPitchRange = 45;
    this.ctrlPitchDeadZone = 5;
    this.ctrlRollRange = 45;
    this.ctrlRollDeadZone = 5;
    this.ctrlRotationCompensator = true;
    this.sound = true;
    this.vibrate = true;
};
